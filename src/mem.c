/*****************************************************
 * Copyright Grégory Mounié 2008-2013                *
 * This code is distributed under the GLPv3 licence. *
 * Ce code est distribué sous la licence GPLv3+.     *
 *****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "mem.h"

/** squelette du TP allocateur memoire */

void *zone_memoire = 0;

//
typedef struct ListeBuddy
{
    struct ListeBuddy *Suiv;
} *Liste;

// initialisation de la table des zones libres
// chaque case represente une puissance de 2: min 2^0 / max 2^20
Liste tzl[21];

// fonction d'initialisation du tableau tzl
void
init_tzl(Liste tzl)
{
  int i = 0;
  for (i = 0 ; i < 21 ; i++)
  {
    tzl(i) = NULL;
  }
}

// fonction retournant la puissance de 2 superieure ou égale à l'entree
int
get_puissance(unsigned long size)
{
  int k = 0;
  unsigned long puiss = 1;
  // si la taille est negatif -> cas d'erreur
  if (size <= 0)
  {
    return -1;
  }
  // si la taille est 1: case 2^0
  if (size == 1)
  {
    return 0;
  }
  while (k < 20) {
    k++; // on passe à la puissance supérieure
    puiss = puiss * 2;
    if (size <= puiss)
    {
      return k;
    }
  }
  // cas où on ne trouve pas de puissance de deux suffisante
  return -1; 
}

// fonction qui place une zone mémoire au bon endroit dans le tzl.
// replace_mem est appelée par mem_free qui transforme size en k afin
// de simplifier les calculs à faire potentiellement.
int
replace_mem(void *ptr, int k)
{
  Liste parcours = tzl+k; // parcours pointe sur le (k+1)ème élément de tzl
  void * Buddy =ptr + ( (ptr - zone_memoire) ^ (1<<k) ); // Buddy de ptr
  while (parcours != NULL)
  {
    if (*parcours == Buddy) // les deux éléments peuvent être fussionés
    {
      parcours = *Buddy; // on enlève Buddy de la liste
      // on place la nouvelle mémoire (ptr + Buddy) dans la liste de 
      // taille supérieure par itération
      return replace_mem(ptr + ( (ptr - zone_memoire) | !(1<<k) ),k+1); 
    }
    parcours = parcours->Suiv;
  }
  // on ne trouve pas le buddy, on ajoute donc ptr à la liste.
  // on ajoute en tête pour simplifier
  *ptr = tzl[k]; // ptr pointe le suivant dans la liste
  tzl[k] = ptr; // et on le met en première position
  return 0; 
}

// fonction pour creer une zone de memoire libre de taille demandee
Liste
creer_mem(int taille)
{
  if (tzl[taille])
  {
      while( k < taille) {
          tzl[k] = zone_memoire + 2**(k-1) - 1;
  }
  else
  {
    return creer_mem(taille + 1);
  }
}

int 
mem_init()
{
  if (! zone_memoire)
    zone_memoire = (void *) malloc(ALLOC_MEM_SIZE);
  if (zone_memoire == 0)
    {
      perror("mem_init:");
      return -1;
    }
  // initialisation de la table des zones libres: dans tout les cases on a 'null'
  init_tzl(tzl);
  // l'adresse de la  zone memoire alloue est dans la case qui represente 2^20
  tzl[20] = zone_memoire;
  return 0;
}

void *
mem_alloc(unsigned long size)
{
  // teste si la largeur est possible et trouve une zone de memoire adequate
  int taille = get_puissance(size);
  if (taille == -1)
  {
    return 0;
  }
  if (tzl[taille])
  {
    return tzl + taille;
  }
  // zone pas encore cree -> tzl[taille] est egal à NULL
  else
  {
    int j;
    for (j = taille; j < 20; j++)
    {
        if (tzl[j])
        {
          break;
        }
    }
    int k = 20;
    while (k > taille)
    {
    
    }
}

int 
mem_free(void *ptr, unsigned long size)
{
  int k = get_puissance(size);
  if (k == -1)
  {
    return -1;
  }
  return replace_mem(ptr, k);
}


int
mem_destroy()
{
  

  free(zone_memoire);
  zone_memoire = 0;
  return 0;
}

